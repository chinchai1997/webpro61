<!DOCTYPE html>
<html>

<body>
  <script>
    'use strict';
    let str = 'Hi';
    
    str[0] = 'h'; // error
    alert( str[0] ); // doesn't work
  </script>
</body>

</html>