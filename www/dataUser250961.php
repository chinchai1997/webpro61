<!DOCTYPE html>
<html>

<body>
  <script>
    'use strict';
    function makeUser(name, age) {
      return {
        name: name,
        age: age
        // ...other properties
      };
    }
    
    let user = makeUser("John", 30);
    alert(user.name); // John
  </script>
</body>

</html>